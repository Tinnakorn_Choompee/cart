@extends('layouts.backend') 
@section('title', 'เข้าสู่ระบบ | Login')
@section('styles')
{{ Html::style('plugin/iCheck/square/blue.css') }} 
@endsection
@section('login')

<div class="login-logo">
   <b>CNX</b>Phone
</div>
<!-- /.login-logo -->
<div class="login-box-body">
    <p class="login-box-msg font" style="font-size:22px !important;">เข้าสู่ระบบ</p>

    @if ($errors->has('username'))
    <center>
        <span class="invalid-feedback text-danger">
            <span class="font" style="font-size:20px;">{!! $errors->first('username') !!}</span>
        </span>
    </center>
    @endif
    {{ Form::open(['route'=>'login']) }}
    @csrf
        <div class="form-group has-feedback">
            {{ Form::text('username', NULL, ['class'=>'form-control', 'placeholder'=>'Username']) }}
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="Password">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            @if ($errors->has('password'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
        </div>
        <div class="row">
            <div class="col-xs-6">
                <div class="checkbox icheck" style="margin-top:14px;">
                    <label>
                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> 
                        <span style="vertical-align: middle !important;"> {{ __('Remember Me') }} </span> 
                    </label>
                </div>
            </div>
            <!-- /.col -->
            <div class="col-xs-6">
                <button type="submit" class="btn btn-primary btn-block btn-flat">  {{ __('Login') }} </button>
            </div>
            <!-- /.col -->
        </div>
    {{ Form::close() }}
</div>
<!-- /.login-box-body -->
@endsection

@push('scripts')
<!-- iCheck -->
{{ Html::script('plugin/iCheck/icheck.min.js') }}
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' /* optional */
    });
  });
</script>
@endpush
@extends('layouts.backend') 
@section('title', 'แก้ไขผู้ใช้งานระบบ | User Edit') 
@section('content')
<!-- Content Wrapper Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1 class"menu">
			แก้ไขผู้ใช้งานระบบ | 
			<small> User Edit</small>
		</h1>
			{{ Breadcrumbs::render() }}
	</section>
	<!-- Main content -->
	<section class="content">
		<div class="row">
			<!-- left column -->
			<div class="col-md-12 col-xs-12">
				<!-- general form elements -->
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title font">ข้อมูลผู้ใช้งานระบบ</h3>
					</div>
					@if ($errors->any())
					<div class="alert alert-danger">
						<ul>
							@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
					@endif
					<!-- /.box-header -->
					<!-- form start -->
                    {!! Form::model($user, ['method' => 'PATCH','route' => ['users.update', $user->id], 'files'=>TRUE]) !!}
					<div class="box-body">
                        <div class="row">
                            <div class="form-group">
                                {!! Form::label('name', 'ชื่อ : ', ['class'=>'label_font col-sm-4 text-right']) !!}
                                <div class="col-sm-4">
                                    {!! Form::text('name', NULL, ['placeholder' => 'ชื่อ', 'class'=>'form-control', 'required']) !!}
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">   
                            <div class="form-group">
                                {!! Form::label('email', 'อีเมล์ : ', ['class'=>'label_font col-sm-4 text-right']) !!}
                                <div class="col-sm-4">
                                    {!! Form::email('email', NULL, ['placeholder' => 'อีเมล์', 'class'=>'form-control', 'required']) !!}
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">   
                            <div class="form-group">
                                {!! Form::label('username', 'ชื่อเข้าใช้ระบบ : ', ['class'=>'label_font col-sm-4 text-right']) !!}
                                <div class="col-sm-4">
                                    {!! Form::text('username', NULL, ['placeholder' => 'ชื่อเข้าใช้ระบบ', 'class'=>'form-control', 'required']) !!}
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">   
                            <div class="form-group">
                                {!! Form::label('password', 'พาสเวิร์ด : ', ['class'=>'label_font col-sm-4 text-right']) !!}
                                <div class="col-sm-4">
                                    {!! Form::password('password', ['placeholder' => 'พาสเวิร์ด','class'=>'form-control']) !!}
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">   
                            <div class="form-group">
                                {!! Form::label('confirm-password', 'ยืนยันพาสเวิร์ด : ', ['class'=>'label_font col-sm-4 text-right']) !!}
                                <div class="col-sm-4">
                                    {!! Form::password('confirm-password', ['placeholder' => 'ยืนยันพาสเวิร์ด','class' => 'form-control']) !!}
                                </div>
                            </div>
                        </div>
                        <br>    
                        <div class="row">   
                            <div class="form-group">
                                {!! Form::label('radio_role', 'สิทธิ์ผู้ใช้งาน : ', ['class'=>'label_font col-sm-4 text-right']) !!}
                                <div class="col-sm-4">
                                    {!! Form::select('roles[]', $roles,$userRole, array('class' => 'form-control','multiple')) !!}
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">   
                            <div class="form-group is-fileinput">
                                {!! Form::label('image', 'รูปภาพ : ', ['class'=>'label_font col-sm-4 text-right']) !!}
                                <div class="col-sm-4">
                                    {!! Form::hidden('edit_image', $user->image) !!}
                                    {!! Form::text( NULL,  NULL, ['class'=>'form-control','readonly', 'placeholder'=>'เลือกรูปภาพ']) !!}
                                    {!! Form::file('image', ['class'=>'form-control','id'=> 'image']); !!}
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">   
                            <div class="form-group is-fileinput">
                                    {!! Form::label('preview', 'Preview : ', ['class'=>'label_font col-sm-4 text-right']) !!}
                                    <div class="col-sm-4">
                                    {!! Html::image('image/backend/users/'.$user->image, NULL, ['width'=>'55%','height'=>'auto', 'class'=>'rounded img-responsive', 'id'=>'preview']) !!}   
                                </div>
                            </div>
                        </div>
                        <br>
                    </div>
					<!-- /.box-body -->
					<div class="box-footer">
						<button type="submit" class="btn btn-primary label_font"> บันทึก </button>
					</div>
					{!! Form::close() !!}
				</div>
				<!-- /.box -->
			</div>
			<!-- /.row -->
	</section>
    <!-- /.content -->
</div>
@endsection 
@push('scripts')
    <script>
    $.ajaxSetup({
       headers: {
           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
       }
    });
    function readImage(input) {
        if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#preview').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
        }
    }
    $("#image").change(function() {
        readImage(this);
    });
   </script>
@endpush  
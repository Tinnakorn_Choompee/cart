@extends('layouts.backend') 
@section('title', 'ข้อมูลผู้ใช้ระบบ | User Show') 
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1 class"menu">
              ข้อมูลผู้ใช้ระบบ | 
            <small> User Show</small>
          </h1>
            {{ Breadcrumbs::render() }}
        </section>
        <!-- Main content -->
        <section class="content">

                <div class="row">
                  <div class="col-md-3">
          
                    <!-- Profile Image -->
                    <div class="box box-primary">
                      <div class="box-body box-profile">
                        {{ Html::image('image/backend/users/'.$user->image , $user->image , ['class'=>'profile-user-img img-responsive img-circle', 'style'=>"margin-top:15px"]) }}
                        <br>
                        <h3 class="profile-username text-center font" style="font-size:26px;margin-top:20px"> {{ $user->name }} </h3>
                        <br>
                        <p class="text-muted text-center font" style="font-size:24px;padding-bottom:12px;">   {{ $user->email }} </p>
          
                        {{--  <ul class="list-group list-group-unbordered">
                          <li class="list-group-item">
                            <b>Followers</b> <a class="pull-right">1,322</a>
                          </li>
                          <li class="list-group-item">
                            <b>Following</b> <a class="pull-right">543</a>
                          </li>
                          <li class="list-group-item">
                            <b>Friends</b> <a class="pull-right">13,287</a>
                          </li>
                        </ul>
          
                        <a href="#" class="btn btn-primary btn-raised btn-block"><b>Follow</b></a>  --}}
                      </div>
                      <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
                <div class="col-md-9">
                    <!-- About Me Box -->
                    <div class="box box-primary">
                      <div class="box-header with-border">
                       <h3 class="box-title font" style="margin-top:-60px !important">ข้อมูลส่วนตัว</h3>
                      </div>
                      <!-- /.box-header -->
                      <div class="box-body">
                        <strong><i class="fa fa-user-o margin-r-5"></i> Username</strong>
          
                        <p class="text-muted">
                            <br>
                            {{ $user->username }}
                        </p>
          
                        <hr>
          
                        <strong><i class="fa fa-user margin-r-5"></i> Roles </strong>
           
                        <p class="text-muted"> 
                          <br>
                          @if(!empty($user->getRoleNames()))
                            @foreach($user->getRoleNames() as $v)
                                <label class="badge badge-success">{{ $v }}</label>
                            @endforeach
                          @endif
                        </p>
          
                        <hr>
                       
                      </div>
                      <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
             
                </div>
                <!-- /.row -->
          
            </section>
    </div>
@endsection 
   
   

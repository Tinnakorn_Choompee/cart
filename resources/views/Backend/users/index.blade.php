@extends('layouts.backend') 
@section('title', 'ผู้ใช้งานระบบ | Users')
@section('styles')
<!-- DataTables -->
{{ Html::style('plugin/datatables.net-bs/css/dataTables.bootstrap.min.css') }}
@endsection
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 class"menu">
            ผู้ใช้งานระบบ | Users
        </h1>
            {{ Breadcrumbs::render() }}
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">ตารางผู้ใช้งานระบบ</h3>
                        @can('user-create')
                        <a href="{{ route('users.create') }}" class="btn btn-info btn-lg btn-create"> 
                            <i class="ion ion-android-person-add" style="margin-right:10px;"></i> เพิ่มผู้ใช้งานระบบ
                        </a>
                        @endcan
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="table-users" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>ชื่อ</th>
                                    <th>อีเมล์</th>
                                    <th>ประเภทผู้ใช้ระบบ</th>
                                    <th>รูปภาพ</th>
                                    <th>ตัวเลือก</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($users as $k => $user)
                                <tr>
                                    <td class="text-center">{{ ++$k }}</td>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td class="text-center">
                                        @if(!empty($user->getRoleNames()))
                                            @foreach($user->getRoleNames() as $v)
                                                <label class="badge badge bg-green" style="font-size:20px">{{ $v }}</label>
                                            @endforeach
                                        @endif
                                     </td>
                                    <td class="text-center">{{ Html::image('image/backend/users/'.$user->image, $user->image , ['width'=>'90','height'=>'80','class'=>'rounded img-responsive center']) }}</td>
                                    <td class="text-center">
                                        <a href="{{ route('users.show', $user->id ) }}" data-toggle="tooltip" data-placement="bottom" class="btn btn-primary" title="ดูข้อมูล"> <i class="fa fa-eye" aria-hidden="true"></i> </a>  
                                        @can('user-edit')
                                        <a class="btn btn-warning btn-edit" href="{{ route('users.edit',$user->id) }}" data-toggle="tooltip" data-placement="bottom" title="แก้ไขข้อมูล"><i class="fa fa-pencil"></i></a> 
                                        @endcan
                                        @can('user-delete')
                                        <button class="btn btn-danger btn-del" data-id="{{ $user->id }}" data-toggle="tooltip" data-placement="bottom" title="ลบข้อมูล"><i class="fa fa-trash"></i></button>
                                        {{ Form::open(['method' => 'DELETE', 'route' => ['users.destroy', $user->id], 'id'=>'form-delete-'.$user->id]) }} {{ Form::close() }}     
                                        @endcan                                     
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>#</th>
                                    <th>ชื่อ</th>
                                    <th>อีเมล์</th>
                                    <th>ประเภทผู้ใช้ระบบ</th>
                                    <th>รูปภาพ</th>
                                    <th>ตัวเลือก</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection
@push('scripts')
<!-- DataTables -->
{{ Html::script('plugin/datatables.net/js/jquery.dataTables.min.js') }}
{{ Html::script('plugin/datatables.net-bs/js/dataTables.bootstrap.min.js') }}
<!-- page script -->
 <!-- page script -->
@if (session('success'))
<script>
     swal("Success!", "ทำการบันทึกข้อมูลเรียบร้อยแล้ว", "success");
 </script>
@elseif (session('update'))
 <script>
     swal("Updated!", "ทำการแก้ไขข้อมูลเรียบร้อยแล้ว", "success");
 </script>
@elseif (session('delete'))
<script>
    swal("Deleted!", "ทำการลบข้อมูลเรียบร้อยแล้ว", "success");
</script>
@endif
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('.btn-del').on('click',function(){
    var id = $(this).data('id');
    swal({
            title: "Are you sure?",
            text: "ต้องการที่จะลบ ข้อมูลผู้ใช้ระบบ นี้ใช่หรือไม่ !!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then(willDelete => {
            if (willDelete) { 
                $( "#form-delete-"+id ).submit();
            }
        });
    });
    $(function () {
    $('[data-toggle="tooltip"]').tooltip()
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })

</script>
@endpush
@extends('layouts.backend') 
@section('title', 'ประเภทบทความ | Posts Type') 
@section('styles')
<!-- DataTables -->
{{ Html::style('plugin/datatables.net-bs/css/dataTables.bootstrap.min.css') }}
@endsection
@section('content')
    <!-- Content Wrapper Contains page content -->
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                ประเภทบทความ
                <small> Posts Type </small>
            </h1>
             {{ Breadcrumbs::render() }}
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h4 class="box-title"> ตารางประเภทบทความ </h4>
                            <a href="{{ route('type.create') }}" class="btn btn-info btn-create"> 
                                <i class="ion ion-android-add-circle" style="margin-right:10px;"></i> เพิ่มประเภทบทความ
                            </a>
                        </div>
                        <div class="box-body table-responsive">
                            <table id="example2" class="table table-bordered table-hover">
                                <thead>
                                    <tr class="text-center">
                                        <th width="10%">#</th>
                                        <th width="30%">หัวข้อ</th>
                                        <th width="30%">slug</th>
                                        <th width="40%">ตัวเลือก</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($types as $K => $type)
                                    <tr>
                                        <td class="text-center">{{ ++$K }}</td>
                                        <td class="text-center">{{ $type->name }}</td>
                                        <td class="text-center">{{ $type->slug }}</td>
                                        <td class="text-center">
                                            <a class="btn btn-warning" href="{{ route('type.edit',$type->id) }}" data-toggle="tooltip" data-placement="bottom" title="แก้ไขข้อมูล"><i class="fa fa-pencil"></i></a>                                              
                                            <button class="btn btn-danger btn-del"  data-id="{{ $type->id }}" data-toggle="tooltip" data-placement="bottom" title="ลบข้อมูล"><i class="fa fa-trash"></i></button>
                                            {{ Form::open(['method' => 'DELETE', 'route' => ['type.destroy', $type->id], 'id'=>'form-delete-'.$type->id]) }} {{ Form::close() }}     
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                    <tr class="text-center">
                                        <th width="10%">#</th>
                                        <th width="30%">หัวข้อ</th>
                                        <th width="30%">slug</th>
                                        <th width="40%">ตัวเลือก</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
@endsection 
@push('scripts')
    <!-- DataTables -->
    {{ Html::script('plugin/datatables.net/js/jquery.dataTables.min.js') }} 
    {{ Html::script('plugin/datatables.net-bs/js/dataTables.bootstrap.min.js')}}
    <!-- page script -->
    @if (session('success'))
        <script>
            swal("Success!", "ทำการบันทึกข้อมูลเรียบร้อยแล้ว", "success");
        </script>
    @elseif (session('update'))
        <script>
            swal("Updated!", "ทำการแก้ไขข้อมูลเรียบร้อยแล้ว", "success");
        </script>
    @elseif (session('delete'))
    <script>
        swal("Deleted!", "ทำการลบข้อมูลเรียบร้อยแล้ว", "success");
    </script>
    @endif
    <script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('.btn-del').on('click',function(){
       var id = $(this).data('id');
       swal({
            title: "Are you sure?",
            text: "ต้องการที่จะลบ ประเภทบทความ นี้ใช่หรือไม่ !!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
            })
            .then(willDelete => {
            if (willDelete) { 
                $( "#form-delete-"+id ).submit();
            }
        });
    });

    $('[data-toggle="tooltip"]').tooltip(); 

    $(function () {
            $("#example1").DataTable();
            $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": false,
            "info": false,
            "autoWidth": true
        });
    });
    </script>
@endpush
@extends('layouts.backend') 
@section('title', 'แก้ไขแท็กบทความ | BlogTag Create') 
@section('content')
<!-- Content Wrapper Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1 class"menu">
            แก้ไขแท็กบทความ | 
			<small> Tag Create </small>
		</h1>
			{{ Breadcrumbs::render() }}
	</section>
	<!-- Main content -->
	<section class="content">
		<div class="row">
			<!-- left column -->
			<div class="col-md-12 col-xs-12">
				<!-- general form elements -->
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title font">ข้อมูลแท็กบทความ</h3>
					</div>
					@if ($errors->any())
					<div class="alert alert-danger">
						<ul>
							@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
					@endif
					<!-- /.box-header -->
					<!-- form start -->
					{!! Form::model( $tag, ['method' => 'PATCH', 'route' => ['tag.update', $tag->id]]) !!}
					<div class="box-body">
                        <div class="row">
                            <div class="form-group">
                                {!! Form::label('name', 'Name : ', ['class'=>'label_font col-sm-4 text-right']) !!}
                                <div class="col-sm-4">
                                    {!! Form::text('name', NULL, ['placeholder' => 'name', 'class'=>'form-control', 'required']) !!}
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">   
                            <div class="form-group">
                                {!! Form::label('slug', 'Slug : ', ['class'=>'label_font col-sm-4 text-right']) !!}
                                <div class="col-sm-4">
                                    {!! Form::text('slug', NULL, ['placeholder' => 'slug', 'class'=>'form-control', 'required']) !!}
                                </div>
                            </div>
                        </div>
                        <br>
                    </div>
					<!-- /.box-body -->
					<div class="box-footer">
                        {{ Form::submit('บันทึก', ['class'=>'btn btn-primary label_font']) }}
					</div>
					{!! Form::close() !!}
				</div>
				<!-- /.box -->
			</div>
			<!-- /.row -->
	</section>
    <!-- /.content -->
</div>
@endsection 
@push('scripts')
@endpush
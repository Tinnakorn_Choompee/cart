@extends('layouts.backend') 
@section('title', 'การโพสต์ | Posts') 
@section('styles')
<!-- DataTables -->
{{ Html::style('plugin/datatables.net-bs/css/dataTables.bootstrap.min.css') }}
@endsection
@section('content')
    <!-- Content Wrapper Contains page content -->
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                การโพสต์
                <small> Posts </small>
            </h1>
             {{ Breadcrumbs::render() }}
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h4 class="box-title"> ตารางข้อมูลการโพสต์ </h4>
                            <a href="{{ route('blog.create') }}" class="btn btn-info btn-create"> 
                                <i class="ion ion-android-add-circle" style="margin-right:10px;"></i> เพิ่มการโพสต์
                            </a>
                        </div>
                        <div class="box-body table-responsive">
                            <table id="example2" class="table table-bordered table-hover">
                                <thead>
                                    <tr class="text-center">
                                        <th width="10%">#</th>
                                        <th width="20%">หัวข้อ</th>
                                        <th width="20%">สร้างเมื่อ</th>
                                        <th width="10%">สถานะ</th>
                                        <th width="20%">ตัวเลือก</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                   <tr class="text-center">
                                    <th width="10%">#</th>
                                    <th width="20%">หัวข้อ</th>
                                    <th width="20%">สร้างเมื่อ</th>
                                    <th width="10%">สถานะ</th>
                                    <th width="20%">ตัวเลือก</th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                @foreach($blogs as $K => $blog)
                                    <tr>
                                        <td class="text-center">{{ ++$K }}</td>
                                        <td>{{ $blog->title }}</td>
                                        <td class="text-center">{{ $blog->created_at }}</td>
                                        <td class="text-center">
                                            @if($blog->status == 0)
                                            <span class="badge bg-yellow" style="font-size:16px;"> Unpublic </span>
                                            @else
                                            <span class="badge bg-green" style="font-size:16px;"> Public </span>
                                            @endif
                                        </td>
                                        <td class="text-center">
                                            <a href="{{ route('blog.show', $blog->id ) }}" data-toggle="tooltip" data-placement="bottom" class="btn btn-primary" title="ดูข้อมูล"> <i class="fa fa-eye" aria-hidden="true"></i> </a>  
                                            <a class="btn btn-warning btn-edit" href="{{ route('blog.edit',$blog->id) }}" data-toggle="tooltip" data-placement="bottom" title="แก้ไขข้อมูล"><i class="fa fa-pencil"></i></a> 
                                            <button class="btn btn-danger btn-del" data-id="{{ $blog->id }}" data-toggle="tooltip" data-placement="bottom" title="ลบข้อมูล"><i class="fa fa-trash"></i></button>
                                            {{ Form::open(['method' => 'DELETE', 'route' => ['blog.destroy', $blog->id], 'id'=>'form-delete-'.$blog->id]) }} {{ Form::close() }}     
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
@endsection 
@push('scripts')
    <!-- DataTables -->
    {{ Html::script('plugin/datatables.net/js/jquery.dataTables.min.js') }} 
    {{ Html::script('plugin/datatables.net-bs/js/dataTables.bootstrap.min.js')}}
    <!-- page script -->
    @if (session('success'))
        <script>
            swal("Success!", "ทำการบันทึกข้อมูลเรียบร้อยแล้ว", "success");
        </script>
    @elseif (session('update'))
        <script>
            swal("Updated!", "ทำการแก้ไขข้อมูลเรียบร้อยแล้ว", "success");
        </script>
    @elseif (session('delete'))
    <script>
        swal("Deleted!", "ทำการลบข้อมูลเรียบร้อยแล้ว", "success");
    </script>
    @endif
    <script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('.btn-del').on('click',function(){
       var id = $(this).data('id');
       swal({
            title: "Are you sure?",
            text: "ต้องการที่จะลบ โพสต์ นี้ใช่หรือไม่ !!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
            })
            .then(willDelete => {
            if (willDelete) { 
                $( "#form-delete-"+id ).submit();
            }
        });
    });

    $('[data-toggle="tooltip"]').tooltip(); 

    $(function () {
            $("#example1").DataTable();
            $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": false,
            "info": false,
            "autoWidth": true
        });
    });
    </script>
@endpush
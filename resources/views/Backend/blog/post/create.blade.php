@extends('layouts.backend') 
@section('title', 'เพิ่มการโพสต์ | Posts Create')
@section('styles')
<!-- iCheck for checkboxes and radio inputs -->
{{ Html::style('plugin/iCheck/all.css') }}
<!-- bootstrap wysihtml5 - text editor -->
{{ Html::style('plugin/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}
<!-- Select2 -->
{{ Html::style('plugin/select2/dist/css/select2.min.css') }}
@endsection
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 class"menu">
            เพิ่มการโพสต์ | Posts Create
        </h1>
            {{ Breadcrumbs::render() }}
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                <h3 class="box-title">หัวข้อโพสต์</h3>
                </div>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <!-- /.box-header -->
                <!-- form start -->
                {!! Form::open(['route' => 'blog.store', 'files'=>TRUE]) !!}
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('title', 'หัวข้อ', ['class'=>'label_font']) !!}
                                {!! Form::text('title', NULL, ['class'=>'form-control' ,'required']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('subtitle', 'หัวข้อย่อย ', ['class'=>'label_font']) !!}
                                {!! Form::text('subtitle', NULL, ['class'=>'form-control','required']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('Slug', 'Slug ', ['class'=>'label_font']) !!}
                                {!! Form::text('slug', NULL, ['class'=>'form-control','required']) !!}
                            </div>
                            <div class="form-group">
                                    <br>
                                    <label>
                                        {{ Form::checkbox('public', 1 , false, ['class' => 'minimal'] ) }}
                                        <span class="label_font"> Publish  </span>
                                    </label>
                                </div>
                        </div>
                         <div class="col-md-6">
                             <div class="row">
                                 <div class="col-md-12">
                                    <div class="form-group">
                                        {!! Form::label('type', 'ประเภทบทความ', ['class'=>'label_font']) !!}
                                        {!! Form::select('type[]', $type, null, ['class'=>'form-control select2', 'multiple', 'style'=>'width:100%;']) !!}
                                    </div>
                                    <br>
                                    <div class="form-group">
                                        {!! Form::label('tag', 'แท็กบทความ', ['class'=>'label_font']) !!}
                                        {!! Form::select('tag[]', $tag, null, ['class'=>'form-control select2', 'style'=>'width:100%;', 'multiple']) !!}
                                    </div>
                                 </div>
                                  <div class="col-md-12">
                                      <div class="form-group is-fileinput" style="margin-top:35px;">
                                        {!! Form::label('image', 'รูปภาพ ', ['class'=>'label_font']) !!}
                                        {!! Form::text( NULL,  NULL, ['class'=>'form-control','readonly', 'placeholder'=>'เลือกรูปภาพ', 'id'=>'show_detail']) !!}
                                        {!! Form::file('image', ['class'=>'form-control']); !!}
                                    </div>
                                 </div>
                             </div>
                        </div>
                    </div>
                   
                    <hr>      
                    <h3 class="box-title">รายละเอียด</h3>
                    {{ Form::textarea('body', NULL, ['class'=>'form-control textarea editor' ,'placeholder'=>'Place some text here', 'rows'=> 5, 'cols'=>30]) }}
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                    {{ Form::submit('บันทึก', ['class'=>'btn btn-primary label_font']) }}
                </div>
                {!! Form::close() !!}
            </div>
            <!-- /.box -->
            </div>
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection
@push('scripts')
<!-- iCheck 1.0.1 -->
{{ Html::script('plugin/iCheck/icheck.min.js') }}
<!-- Bootstrap WYSIHTML5 -->
{{ Html::script('plugin/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}
<!-- Select2 -->
{{ Html::script('plugin/select2/dist/js/select2.full.min.js')}}
<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    $('input').iCheck({ checkboxClass: 'icheckbox_minimal-blue',})
    //bootstrap WYSIHTML5 - text editor
    $('.textarea').wysihtml5()
    
    $("#image").change(function() {
        var name = $('input[type=file]').val().split('\\').pop();
        $("#show_detail").val(name);
    });
  });

   
</script>
@endpush
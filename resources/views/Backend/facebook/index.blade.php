@extends('layouts.backend') 
@section('title', 'ตั้งค่าเฟสบุ๊ค | Setting Facebook')
@section('styles')
@endsection
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 class"menu">
            ตั้งค่าเฟสบุ๊ค | Facebook Setting
        </h1>
            {{ Breadcrumbs::render() }}
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="box box-info">
              <div class="box-header with-border">
                <h3 class="box-title"> ข้อมูลการเชื่อมต่อ Fanpage Facebook </h3>
              </div>
               @if ($errors->any())
					<div class="alert alert-danger">
						<ul>
							@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif
              <!-- /.box-header -->
              
              <!-- form start -->
            {{ Form::model($facebook, ['route' => ['facebook.update', $facebook->id], 'method'=>'PATCH', 'class'=>'form-horizontal'])}}
                <div class="box-body">
                  <div class="form-group">
                    {{ Form::label('App_id', 'App_id', ['class'=>'col-sm-3 control-label']) }}
                    <div class="col-sm-7">
                      {{ Form::text('app_id', NULL, ['class'=>'form-control' ,'placeholder'=>'App_id', 'required']) }}
                    </div>
                  </div>
                  <div class="form-group">
                    {{ Form::label('App_secret', 'App_secret', ['class'=>'col-sm-3 control-label']) }}
                    <div class="col-sm-7">
                        {{ Form::text('app_secret', NULL, ['class'=>'form-control' ,'placeholder'=>'App_secret', 'required']) }}
                    </div>
                  </div>
                  <div class="form-group">
                    {{ Form::label('Default_graph_version', 'Default_graph_version', ['class'=>'col-sm-3 control-label']) }}
                    <div class="col-sm-7">
                        {{ Form::text('default_graph_version', NULL, ['class'=>'form-control' ,'placeholder'=>'Default_graph_version', 'required']) }}
                    </div>
                  </div>
                  <div class="form-group">
                    {{ Form::label('Page_access_token', 'Page_access_token', ['class'=>'col-sm-3 control-label']) }}
                    <div class="col-sm-7">
                        {{ Form::textarea('page_access_token', NULL, ['class'=>'form-control' ,'placeholder'=>'Page_access_token', 'rows'=> 5, 'cols'=>30, 'required']) }}
                    </div>
                  </div>
                </div>
                <div class="box-footer">
                  {{ Form::submit('บันทึก', ['class'=>'btn btn-primary label_font']) }}
                </div>
                <!-- /.box-footer -->
             {{ Form::close() }}

            </div>
            <!-- /.box -->
          </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection
@push('scripts')
@if (session('update'))
<script>
     swal("Success!", "ทำการแก้ไขข้อมูลเรียบร้อยแล้ว", "success");
 </script>
@endif
@endpush
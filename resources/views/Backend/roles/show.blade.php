@extends('layouts.backend') 
@section('title', 'ข้อมูลสิทธิ์ผู้ใช้งาน | Role Show') 
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1 class"menu">
                ข้อมูลสิทธิ์ผู้ใช้งาน | 
                <small> Role Show</small>
            </h1>
                {{ Breadcrumbs::render() }}
        </section>
        <!-- Main content -->
        <section class="content">

                <div class="row">
                  <div class="col-md-3">
                    <!-- Profile Image -->
                    <div class="box box-primary">
                      <div class="box-body box-profile">
                            <hr>
                        <br>
                        <h3 class="profile-username text-center font" style="font-size:26px;margin-top:20px">  ชื่อสิทธิ์ผู้ใช้งาน </h3>
                        <br>
                        <p class="text-muted text-center font" style="font-size:24px;padding-bottom:12px;">  {{ $role->name }} </p>
                        <hr>
                      </div>
                      <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
                <div class="col-md-9">
                     <!-- Profile Image -->
                     <div class="box box-primary">
                            <div class="box-body box-profile">
                                <div class="box-header with-border">
                                    <p class="box-title font" style="margin-top:-60px !important">ข้อมูลสิทธิ์ผู้ใช้งาน</p>
                                </div>
                              <p class="text-muted">
                                  <div class="row">
                                    <div class="col-md-6">
                                        @if(!empty($rolePermissions))
                                            @foreach($rolePermissions as $v)
                                              @foreach($action as $k => $rs)
                                                  @if($v->name == $k)
                                                  <p class="font" style="font-size:20px;border-left-style:solid;margin:30px 0px 12px 12px">  <span style="margin-left:10px">{{ $rs }}</span>  </p>
                                                  @endif
                                              @endforeach
                                                  <span class="badge bg-green font" style="font-size:18px;margin:0px 20px; font-weight: normal;padding:10px 15px"> {{ $v->action }} </span>
                                            @endforeach
                                        @endif
                                    </div>
                                  </div>
                              </p>
                              <hr>
                            </div>
                            <!-- /.box-body -->
                          </div>
                          <!-- /.box -->
                    <!-- About Me Box -->
            
                </div>
                <!-- /.col -->
             
                </div>
                <!-- /.row -->
          
            </section>
    </div>
@endsection 
   
   

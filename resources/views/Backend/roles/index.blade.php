@extends('layouts.backend') 
@section('title', 'กำหนดสิทธิ์ผู้ใช้งาน | Role') 
@section('styles')
<!-- DataTables -->
{{ Html::style('plugin/datatables.net-bs/css/dataTables.bootstrap.min.css') }}
@endsection
@section('content')
    <!-- Content Wrapper Contains page content -->
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                กำหนดสิทธิ์ผู้ใช้งาน
                <small> Roles </small>
            </h1>
             {{ Breadcrumbs::render() }}
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h4 class="box-title"> ตารางกำหนดสิทธิ์ผู้ใช้งาน </h4>

                            @can('role-create')
                            <a href="{{ route('roles.create') }}" class="btn btn-info btn-create"> 
                                <i class="ion ion-android-person-add" style="margin-right:10px;"></i> เพิ่มสิทธิ์ผู้ใช้งาน
                            </a>
                            @endcan

                        </div>
                        <div class="box-body table-responsive">
                            <table id="example2" class="table table-bordered table-hover">
                                <thead>
                                    <tr class="text-center">
                                        <th width="10%">#</th>
                                        <th width="50%">ประเภทผู้ใช้งาน</th>
                                        <th width="40%">ตัวเลือก</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($roles as $K => $role)
                                    <tr>
                                        <td class="text-center">{{ ++$K }}</td>
                                        <td class="text-center">{{ $role->name }}</td>
                                        <td class="text-center">
                                            <a href="{{ route('roles.show', $role->id) }}" data-toggle="tooltip" data-placement="bottom" class="btn btn-primary" title="ดูสิทธิ์ผู้ใช้งาน"> <i class="fa fa-eye" aria-hidden="true"></i> </a>
                                            @can('role-edit')
                                            <a class="btn btn-warning" href="{{ route('roles.edit',$role->id) }}" data-toggle="tooltip" data-placement="bottom" title="แก้ไขข้อมูล"><i class="fa fa-pencil"></i></a>                                              
                                            @endcan
                                            @can('role-delete')
                                            <button class="btn btn-danger btn-del"  data-id="{{ $role->id }}" data-toggle="tooltip" data-placement="bottom" title="ลบข้อมูล"><i class="fa fa-trash"></i></button>
                                            {{ Form::open(['method' => 'DELETE', 'route' => ['roles.destroy', $role->id], 'id'=>'form-delete-'.$role->id]) }} {{ Form::close() }}     
                                            @endcan 
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                    <tr class="text-center">
                                        <th width="10%">#</th>
                                        <th width="50%">ประเภทผู้ใช้งาน</th>
                                        <th width="40%">ตัวเลือก</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
@endsection 
@push('scripts')
    <!-- DataTables -->
    {{ Html::script('plugin/datatables.net/js/jquery.dataTables.min.js') }} 
    {{ Html::script('plugin/datatables.net-bs/js/dataTables.bootstrap.min.js')}}
    <!-- page script -->
    @if (session('success'))
        <script>
            swal("Success!", "ทำการบันทึกข้อมูลเรียบร้อยแล้ว", "success");
        </script>
    @elseif (session('update'))
        <script>
            swal("Updated!", "ทำการแก้ไขข้อมูลเรียบร้อยแล้ว", "success");
        </script>
    @elseif (session('delete'))
    <script>
        swal("Deleted!", "ทำการลบข้อมูลเรียบร้อยแล้ว", "success");
    </script>
    @endif
    <script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('.btn-del').on('click',function(){
       var id = $(this).data('id');
       swal({
            title: "Are you sure?",
            text: "ต้องการที่จะลบ สิทธิ์ผู้ใช้งาน นี้ใช่หรือไม่ !!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
            })
            .then(willDelete => {
            if (willDelete) { 
                $( "#form-delete-"+id ).submit();
            }
        });
    });

    $('[data-toggle="tooltip"]').tooltip(); 

    $(function () {
            $("#example1").DataTable();
            $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": false,
            "info": false,
            "autoWidth": true
        });
    });
    </script>
@endpush
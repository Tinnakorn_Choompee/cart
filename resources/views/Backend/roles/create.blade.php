@extends('layouts.backend') 
@section('title', 'เพิ่มสิทธิ์ผู้ใช้งาน | Role Create') 
@section('styles')
<!-- iCheck for checkboxes and radio inputs -->
{{ Html::style('plugin/iCheck/all.css') }}
@endsection
 
@section('content')
<!-- Content Wrapper Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1 class "menu">
			เพิ่มสิทธิ์ผู้ใช้งาน |
			<small> Role Create</small>
		</h1>
		{{ Breadcrumbs::render() }}
	</section>
	<!-- Main content -->
	<section class="content">
		<div class="row">
			<!-- left column -->
			<div class="col-md-12 col-xs-12">
				<!-- general form elements -->
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title font">ข้อมูลสิทธิ์ผู้ใช้งาน</h3>
					</div>
					@if ($errors->any())
					<div class="alert alert-danger">
						<ul>
							@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
					@endif
					<!-- /.box-header -->
					<!-- form start -->
					{!! Form::open(['route' => 'roles.store']) !!}
					<div class="box-body">
						<div class="row">
							<div class="form-group">
								{!! Form::label('name', 'ชื่อสิทธิ์ผู้ใช้งาน : ', ['class'=>'label_font col-sm-3 text-right']) !!}
								<div class="col-sm-5">
									{!! Form::text('name', NULL, ['placeholder' => 'ชื่อ', 'class'=>'form-control', 'required']) !!}
								</div>
							</div>
						</div>
						<br>
						<div class="row">
							<div class="form-group">
								{!! Form::label('radio_role', 'การอนุญาติเข้าใช้งาน : ', ['class'=>'label_font col-sm-3 text-right']) !!}
								<div class="col-sm-5">
									<div class="row">
										<div class="col-md-12">
											@foreach($permission as $value) @foreach($role as $k => $v) @if($value->name == $k)
											<div class="row">
												@if($value->name == "role-list")
												<div class="col-md-12">
													<p class="font" style="font-size:22px;border-left-style:solid;"> <span style="margin-left:5px;">{{ $v }} </span> </p>
												</div>
												@else
												<div class="col-md-12">
													<p class="font" style="font-size:22px;border-left-style:solid;margin-top:20px"> <span style="margin-left:5px;">{{ $v }} </span> </p>
												</div>
												@endif
											</div>
											@endif @endforeach
											<span class="togglebutton" style="margin-left:10px;margin-right:20px;">
												<label>
														{{ Form::checkbox('permission[]', $value->id, false, ['class' => 'minimal'] ) }}
														<span class="label_font"> {{ $value->action }} </span>
												</label>
											</span>
											@endforeach
										</div>
									</div>
								</div>
							</div>
						</div>
						<br>
					</div>
					<!-- /.box-body -->
					<div class="box-footer">
						<button type="submit" class="btn btn-primary label_font"> บันทึก </button>
					</div>
					{!! Form::close() !!}
				</div>
				<!-- /.box -->
			</div>
			<!-- /.row -->
	</section>
	<!-- /.content -->
	</div>
@endsection
 @push('scripts')
	<!-- iCheck 1.0.1 -->
	{{ Html::script('plugin/iCheck/icheck.min.js') }}
	<script>
		$(document).ready(function(){
   		//iCheck for checkbox and radio inputs
		$('input').iCheck({
		checkboxClass: 'icheckbox_minimal-blue',
		})
	});

	</script>
	
@endpush
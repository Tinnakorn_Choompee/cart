@extends('layouts.backend') 
@section('title', '404 Page') 
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      404 Error Permission Page
    </h1>
  </section>

  <!-- Main content -->
  <section class="content" style="margin-top:5%;">
    <div class="error-page">
      <h2 class="headline text-yellow"> 404</h2>
      <div class="error-content">
        <h3><i class="fa fa-warning text-yellow"></i> Oops! Permission Page </h3>
        <p class="menu" style="font-size:30px;">
            คุณไม่สิทธิ์ในการใช้งานหน้านี้ 
        </p>
        <button onclick="goBack()" class="btn btn-warning btn-lg"><span class="ion ion-arrow-left-a"></span> Go Back </button>
      </div>
      <!-- /.error-content -->
    </div>
    <!-- /.error-page -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection
@push('scripts')
<script>
  function goBack() { window.history.back() }
</script>
@endpush
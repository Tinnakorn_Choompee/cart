<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>
    <link rel="icon" href="/image/backend/favicon.png" type="image/x-icon" />
    <!-- Bootstrap 3.3.7 -->
    {{ Html::style('plugin/bootstrap/dist/css/bootstrap.min.css') }}
    <!-- Font Awesome -->
    {{ Html::style('plugin/font-awesome/css/font-awesome.min.css') }}
    <!-- Ionicons -->
    {{ Html::style('plugin/Ionicons/css/ionicons.min.css') }}
    <!-- Prompt Font -->
    {{ Html::style('https://fonts.googleapis.com/css?family=Prompt') }}
    <!-- Theme style -->
    {{ Html::style('css/backend/AdminLTE.min.css') }}
    <!-- Material Design -->
    {{ Html::style('css/backend/bootstrap-material-design.min.css') }}
    {{-- {{ Html::style('css/backend/ripples.min.css') }} --}}
    {{ Html::style('css/backend/MaterialAdminLTE.min.css') }}
    <!-- AdminLTE Skins. Choose a skin from the css/skins -->
    {{ Html::style('css/backend/skins/_all-skins.min.css') }}
    <!-- Style -->
    {{ Html::style('css/backend/style.css') }}
    @yield('styles')
</head>
    @auth
        <body class="hold-transition skin-blue sidebar-mini">
            <div class="wrapper">
                @include('layouts.template.backend.header')
                @include('layouts.template.backend.aside')
                @yield('content')
                @include('layouts.template.backend.footer')
            </div>
            <!-- ./wrapper -->
    @endauth
    @guest
        <body class="hold-transition login-page">
            <div class="login-box">
                @yield('login')
            </div>
            <!-- /.login-box -->
    @endguest   
    <!-- jQuery 3 -->
    {{ Html::script('plugin/jquery/dist/jquery.min.js') }}
    <!-- jQuery UI 1.11.4 -->
    {{ Html::script('plugin/jquery-ui/jquery-ui.min.js') }}
    <script>
        $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap 3.3.7 -->
    {{ Html::script('plugin/bootstrap/dist/js/bootstrap.min.js') }}
    <!-- Material Design -->
    {{ Html::script('js/backend/material.min.js') }}
    {{ Html::script('js/backend/ripples.min.js') }}
    <!-- Slimscroll -->
    {{ Html::script('plugin/jquery-slimscroll/jquery.slimscroll.min.js') }}
    <!-- FastClick -->
    {{ Html::script('plugin/fastclick/lib/fastclick.js') }}
    <!-- Sweetalert -->
    {{ Html::script('plugin/sweetalert/sweetalert.min.js') }} 
    @stack('scripts')
    <!-- AdminLTE App -->
    {{ Html::script('js/backend/adminlte.min.js') }}
</body>
</html>

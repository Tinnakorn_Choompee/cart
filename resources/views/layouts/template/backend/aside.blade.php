@php 
use Libraries\MenuLibrary\MenuLibrary;
@endphp
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                {{ Html::image('image/backend/users/'.Auth::user()->image, NULL, ['class'=>'img-circle']) }}
            </div>
            <div class="pull-left info">
                <p>  {{ Auth::user()->name }} </p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        {{-- <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                    <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                    </button>
                </span>
            </div>
        </form>
        <!-- /.search form --> --}}
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <!--------------------- MENU ---------------------------->
            <li class="header">MENU</li>
            @foreach(MenuLibrary::Menu() as $k => $v)
                @if($v['submenu'] == FALSE)
                    @if(str_contains(Request::url(), $v['url']))
                    <li class="active">
                        <a href="{{ $v['route'] }}">
                            <i class="fa {{ $v['icon'] }}"></i>
                            <span class="menu"> {{ $v['name'] }}  </span>
                        </a>
                    </li>
                    @else
                    <li>
                        <a href="{{ $v['route'] }}">
                            <i class="fa {{ $v['icon'] }}"></i>
                            <span class="menu"> {{ $v['name'] }}  </span>
                        </a>
                    </li>
                    @endif
                @else

                    
                @if(str_contains(Request::url(), $v['name']))
                <li class="treeview active">
                @else
                <li class="treeview">
                @endif
                    <a href="#">
                        <i class="fa fa-file-text-o"></i>
                        <span>{{ $v['title'] }}</span>
                        <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        @foreach($v['list'] as $k => $r)
                            @if(str_contains(Request::url(), $r['url']))
                            <li class="active">
                                <a href="{{ $r['route'] }}"><i class="fa fa-circle-o"></i> 
                                    <span class="menu">{{ $k }}</span>  
                                </a>
                            </li>
                            @else
                            <li>
                                <a href="{{ $r['route'] }}"><i class="fa fa-circle-o"></i> 
                                    <span class="menu">{{ $k }}</span>  
                                </a>
                            </li>
                            @endif
                        @endforeach
                    </ul>
                </li>
                @endif
            @endforeach
            
            <!--------------------- SETTING ---------------------------->
            <li class="header">SETTING</li>
            @foreach(MenuLibrary::Setting() as $k => $v)
            @if(str_contains(Request::url(), $v['url']))
            <li class="active">
				<a href="{{ $v['route'] }}">
					<i class="fa {{ $v['icon'] }}"></i>
					<span class="menu"> {{ $v['name'] }}  </span>
				</a>
            </li>
            @else
            <li>
				<a href="{{ $v['route'] }}">
					<i class="fa {{ $v['icon'] }}"></i>
					<span class="menu"> {{ $v['name'] }}  </span>
				</a>
            </li>
            @endif
            @endforeach
            <!--------------------- USER ---------------------------->
            <li class="header">USER</li>
            @foreach(MenuLibrary::User() as $k => $v)
            @can($v['role'])
            @if(str_contains(Request::url(), $v['url']))
			<li class="active">
				<a href="{{ $v['route'] }}">
					<i class="fa {{ $v['icon'] }}"></i>
					<span class="menu"> {{ $v['name'] }}  </span>
				</a>
            </li>
            @else
            <li>
				<a href="{{ $v['route'] }}">
					<i class="fa {{ $v['icon'] }}"></i>
					<span class="menu"> {{ $v['name'] }}  </span>
				</a>
            </li>
            @endif
			@endcan
            @endforeach
            <!--------------------- PROFILE ---------------------------->
            <li class="header">PROFILE </li>
			@if(str_contains(Request::route()->getName(), 'profile'))
			<li class="active">
				<a href="{{ route('profile', Auth::user()->id) }}">
					<i class="ion ion-person"></i>
					<span class="menu"> ข้อมูลส่วนตัว  </span>
				</a>
			</li>
			@else
			<li>
				<a href="{{ route('profile', Auth::user()->id) }}">
					<i class="ion ion-person"></i>
					<span class="menu"> ข้อมูลส่วนตัว  </span>
				</a>
			</li>
			@endif
			<li>
			<a href="javascript:{}" onclick="document.getElementById('logout-form-sidebar').submit();">
				{!! Form::open(['url' => 'logout', 'id'=>'logout-form-sidebar']) !!}{!! Form::close() !!}
				<i class="ion ion-android-exit"></i>	
				<span class="menu"> ออกจากระบบ </span>		
			</a>
			</li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
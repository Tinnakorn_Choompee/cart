<header class="main-header">
    <!-- Logo -->
    <a href="{{ route('home') }}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>C</b>NX</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>CNX</b>Phone</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- Messages: style can be found in dropdown.less-->
                {{-- <li class="dropdown messages-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-envelope-o"></i>
                <span class="label label-success">4</span>
                </a>
                    <ul class="dropdown-menu">
                        <li class="header">You have 4 messages</li>
                        <li>
                            <!-- inner menu: contains the actual data -->
                            <ul class="menu">
                                <li>
                                    <!-- start message -->
                                    <a href="#">
                                        <div class="pull-left">
                                            {{ Html::image('image/backend/user2-160x160.jpg', NULL, ['class'=>'img-circle']) }}
                                        </div>
                                        <h4>
                                            Support Team
                                            <small><i class="fa fa-clock-o"></i> 5 mins</small>
                                        </h4>
                                        <p>Why not buy a new awesome theme?</p>
                                    </a>
                                </li>
                                <!-- end message -->
                                <li>
                                    <a href="#">
                                        <div class="pull-left">
                                            {{ Html::image('image/backend/user3-128x128.jpg', NULL, ['class'=>'img-circle']) }}
                                        </div>
                                        <h4>
                                            AdminLTE Design Team
                                            <small><i class="fa fa-clock-o"></i> 2 hours</small>
                                        </h4>
                                        <p>Why not buy a new awesome theme?</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="pull-left">
                                            {{ Html::image('image/backend/user4-128x128.jpg', NULL, ['class'=>'img-circle']) }}
                                        </div>
                                        <h4>
                                            Developers
                                            <small><i class="fa fa-clock-o"></i> Today</small>
                                        </h4>
                                        <p>Why not buy a new awesome theme?</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="pull-left">
                                            {{ Html::image('image/backend/user3-128x128.jpg', NULL, ['class'=>'img-circle']) }}
                                        </div>
                                        <h4>
                                            Sales Department
                                            <small><i class="fa fa-clock-o"></i> Yesterday</small>
                                        </h4>
                                        <p>Why not buy a new awesome theme?</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="pull-left">
                                            {{ Html::image('image/backend/user4-128x128.jpg', NULL, ['class'=>'img-circle']) }}
                                        </div>
                                        <h4>
                                            Reviewers
                                            <small><i class="fa fa-clock-o"></i> 2 days</small>
                                        </h4>
                                        <p>Why not buy a new awesome theme?</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="footer"><a href="#">See All Messages</a></li>
                    </ul>
                </li> --}}
                <!-- Notifications: style can be found in dropdown.less -->
                {{-- <li class="dropdown notifications-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-bell-o"></i>
                <span class="label label-warning">10</span>
                </a>
                    <ul class="dropdown-menu">
                        <li class="header">You have 10 notifications</li>
                        <li>
                            <!-- inner menu: contains the actual data -->
                            <ul class="menu">
                                <li>
                                    <a href="#">
                        <i class="fa fa-users text-aqua"></i> 5 new members joined today
                        </a>
                                </li>
                                <li>
                                    <a href="#">
                        <i class="fa fa-warning text-yellow"></i> Very long description here that may not fit into the
                        page and may cause design problems
                        </a>
                                </li>
                                <li>
                                    <a href="#">
                        <i class="fa fa-users text-red"></i> 5 new members joined
                        </a>
                                </li>
                                <li>
                                    <a href="#">
                        <i class="fa fa-shopping-cart text-green"></i> 25 sales made
                        </a>
                                </li>
                                <li>
                                    <a href="#">
                        <i class="fa fa-user text-red"></i> You changed your username
                        </a>
                                </li>
                            </ul>
                        </li>
                        <li class="footer"><a href="#">View all</a></li>
                    </ul>
                </li> --}}
                <!-- Tasks: style can be found in dropdown.less -->
                {{-- <li class="dropdown tasks-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-flag-o"></i>
                <span class="label label-danger">9</span>
                </a>
                    <ul class="dropdown-menu">
                        <li class="header">You have 9 tasks</li>
                        <li>
                            <!-- inner menu: contains the actual data -->
                            <ul class="menu">
                                <li>
                                    <!-- Task item -->
                                    <a href="#">
                                        <h3>
                                            Design some buttons
                                            <small class="pull-right">20%</small>
                                        </h3>
                                        <div class="progress xs">
                                            <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                                <span class="sr-only">20% Complete</span>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <!-- end task item -->
                                <li>
                                    <!-- Task item -->
                                    <a href="#">
                                        <h3>
                                            Create a nice theme
                                            <small class="pull-right">40%</small>
                                        </h3>
                                        <div class="progress xs">
                                            <div class="progress-bar progress-bar-green" style="width: 40%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                                <span class="sr-only">40% Complete</span>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <!-- end task item -->
                                <li>
                                    <!-- Task item -->
                                    <a href="#">
                                        <h3>
                                            Some task I need to do
                                            <small class="pull-right">60%</small>
                                        </h3>
                                        <div class="progress xs">
                                            <div class="progress-bar progress-bar-red" style="width: 60%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                                <span class="sr-only">60% Complete</span>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <!-- end task item -->
                                <li>
                                    <!-- Task item -->
                                    <a href="#">
                                        <h3>
                                            Make beautiful transitions
                                            <small class="pull-right">80%</small>
                                        </h3>
                                        <div class="progress xs">
                                            <div class="progress-bar progress-bar-yellow" style="width: 80%" role="progressbar" aria-valuenow="20" aria-valuemin="0"
                                                aria-valuemax="100">
                                                <span class="sr-only">80% Complete</span>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <!-- end task item -->
                            </ul>
                        </li>
                        <li class="footer">
                            <a href="#">View all tasks</a>
                        </li>
                    </ul>
                </li> --}}
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                {{ Html::image('image/backend/users/'.Auth::user()->image, NULL, ['class'=>'user-image']) }}
                <span class="hidden-xs">  {{ Auth::user()->name }} </span>
                </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            {{ Html::image('image/backend/users/'.Auth::user()->image, NULL, ['class'=>'img-circle']) }}
                            <p>
                                {{ Auth::user()->name }}
                                <small> {{ Auth::user()->email }} </small>
                            </p>
                        </li>
                        <!-- Menu Body -->
                        <li class="user-body">
                            <div class="row">
                                <button data-id="{{ Auth::user()->id }}" data-toggle="modal" data-target="#change_password" class="btn btn-block btn-info font btn-password" style="font-size:20px"> เปลี่ยนรหัสผ่าน </button>
                            </div>
                            <!-- /.row -->
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="{{ route('profile', Auth::user()->id) }}" class="btn btn-default btn-flat">Profile</a>
                            </div>
                            <div class="pull-right">
                                <a href="{{ route('logout') }}" class="btn btn-default btn-flat" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>                                {!! Form::open(['url' => 'logout', 'id'=>'logout-form']) !!} {!! Form::close() !!}
                            </div>
                        </li>
                    </ul>
                </li>
                <!-- Control Sidebar Toggle Button -->
                {{-- <li>
                    <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                </li> --}}
            </ul>
        </div>
    </nav>
</header>

<!-- Modal Change Password -->
<div id="change_password" class="modal fade" role="dialog">
	<div class="modal-dialog" style="margin-top:10%">
		  <!-- Modal content-->
		<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title font" style="font-size:22px"> เปลี่ยนรหัสผ่าน
			<span class="pull-right" id="alert" style="margin-right:20px">
				@if ($errors->has('password_user'))
				<span class="help-block text-danger" style="font-size:22px">
					{{ $errors->first('password_user') }}
				</span>
				@endif
			</span>
			</h4>
		</div>
		<div class="modal-body">
			{!! Form::open(['route' => 'password', 'method' => 'PATCH', 'class'=>'form-horizontal']) !!}
			{!! Form::hidden('id', NULL, ['id'=>'id']) !!}
			<div class="form-group">
				{!! Form::label('password', 'รหัสผ่านใหม่', ['class'=>'col-sm-3 control-label label_font', 'style'=> 'top:-15px']) !!}
				<div class="col-sm-8">
					{!! Form::password('password_user', ['class'=>'form-control', 'required']) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('password_confirmation', 'ยืนยันรหัสผ่าน', ['class'=>'col-sm-3 control-label label_font', 'style'=> 'top:-15px']) !!}
				<div class="col-sm-8">
					{!! Form::password('password_confirmation', ['class'=>'form-control', 'required']) !!}
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default label_font" data-dismiss="modal">ปิดหน้าต่าง</button>
			<button type="submit" class="btn btn-primary label_font">บันทึก</button>
		</div>
		{!! Form::close() !!}
		</div>
	</div>
</div>

@push('scripts')

@if ($errors->has('password_user'))
	<script>
		$('#change_password').modal('show');
	</script>
@endif

@if (session('password_success'))
<script>
	swal("Changed!", "ทำการเปลี่ยนแปลงพาสเวิร์ดเรียบร้อยแล้ว", "success");
</script>
@endif

<script>
	$('#change_password').on('show.bs.modal', function (event) {
        var button  = $(event.relatedTarget)
        var id      = button.data('id')
        $(this).find('.modal-body #id').val(id)
	});
</script>
@endpush
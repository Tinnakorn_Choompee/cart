<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="robots" content="all,follow">
    <meta name="googlebot" content="index,follow,snippet,archive">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Obaju e-commerce template">
    <meta name="author" content="Ondrej Svestka | ondrejsvestka.cz">
    <meta name="keywords" content="">
    <title>@yield('title')</title>
    <link rel="icon" href="/image/backend/favicon.png" type="image/x-icon" />
    <!-- favicon -->
    <link rel="shortcut icon" href="/image/frontend/favicon.png">

    <!-- styles -->
    {{ Html::style('css/frontend/font-awesome.css') }} 
    {{ Html::style('css/frontend/bootstrap.min.css') }} 
    {{ Html::style('css/frontend/animate.min.css')}} 
    {{ Html::style('css/frontend/owl.carousel.css') }} 
    {{ Html::style('css/frontend/owl.theme.css') }}

    <!-- theme stylesheet -->
    {{ Html::style('css/frontend/style.default.css') }}

    <!-- your stylesheet with modifications -->
    {{ Html::style('css/frontend/custom.css') }}

</head>

<body>
    <!-- *** TOPBAR *** -->
    @include('layouts.template.frontend.topbar')
    <!-- *** NAVBAR *** -->
    @include('layouts.template.frontend.navbar')

    <div id="all">
        @yield('content')
        <!-- *** FOOTER *** -->
        @include('layouts.template.frontend.footer')
        <!-- /#footer -->
        <!-- *** FOOTER END *** -->

        <!-- *** COPYRIGHT *** -->
        @include('layouts.template.frontend.copyright')
        <!-- *** COPYRIGHT END *** -->

    </div>
    <!-- /#all -->

    <!-- *** SCRIPTS TO INCLUDE *** -->
    {{ Html::script('js/frontend/respond.min.js') }}
    {{ Html::script('js/frontend/jquery-1.11.0.min.js') }}
    {{ Html::script('js/frontend/bootstrap.min.js') }}
    {{ Html::script('js/frontend/jquery.cookie.js') }}
    {{ Html::script('js/frontend/waypoints.min.js') }}
    {{ Html::script('js/frontend/modernizr.js') }}
    {{ Html::script('js/frontend/bootstrap-hover-dropdown.js') }}
    {{ Html::script('js/frontend/owl.carousel.min.js') }}
    {{ Html::script('js/frontend/front.js') }}
</body>

</html>
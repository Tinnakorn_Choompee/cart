<?php

namespace App\Http\Controllers;

use App\Facebook;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FacebookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Backend.facebook.index')->withFacebook(Facebook::find(1));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Facebook  $facebook
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Facebook $facebook)
    {
        $this->validate($request, [
            'app_id'     => 'required',
            'app_secret' => 'required',
            'default_graph_version' => 'required',
            'page_access_token'     => 'required',
        ]);

        $facebook = Facebook::find($facebook->id);
        $facebook->app_id      = $request->app_id;
        $facebook->app_secret  = $request->app_secret;
        $facebook->default_graph_version = $request->default_graph_version;
        $facebook->page_access_token     = $request->page_access_token;
        $facebook->save();
        return redirect()->route('facebook.index')->with('update','Facebook updated successfully');    
    }
}

<?php

namespace App\Http\Controllers;

use App\Type;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Backend.blog.type.index')->withTypes(Type::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Backend.blog.type.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:types|max:255',
            'slug' => 'required',
        ]);
        $type = new Type;
        $type->name = $request->name;
        $type->slug = $request->slug;
        $type->save();
        return redirect()->route('type.index')->with('success','Type updated successfully');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Type  $Type
     * @return \Illuminate\Http\Response
     */
    public function edit(Type $type)
    {
        return view('Backend.blog.type.edit')->withType($type);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Type  $Type
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Type $type)
    {
        $request->validate([
            'name' => 'required|unique:Tags|max:255',
            'slug' => 'required',
        ]);
        $type->name = $request->name;
        $type->slug = $request->slug;
        $type->save();
        return redirect()->route('type.index')->with('update','Type updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Type  $Type
     * @return \Illuminate\Http\Response
     */
    public function destroy(Type $type)
    {
        Type::destroy($type->id);
        return redirect()->route('type.index')->with('delete','Type deleted successfully');
    }
}

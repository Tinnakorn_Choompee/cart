<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Intervention\Image\ImageManagerStatic as Image;
use DB;
use Hash;
use File;
use Auth;

class UserController extends Controller
{
    function __construct()
    {
         $this->middleware('permission:user-list');
         $this->middleware('permission:user-create', ['only' => ['create','store']]);
         $this->middleware('permission:user-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:user-delete', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Backend.users.index')->withUsers(User::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::pluck('name','name')->all();
        return view('Backend.users.create')->withRoles($roles);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'username.unique' => 'ชื่อเข้าใช้งานซ้ำ',
            'email.unique'    => 'อีเมล์ซ้ำกัน',
        ];
        
        $this->validate($request, [
            'name' => 'required',
            'username' => 'required|unique:users',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|same:confirm-password',
            'roles' => 'required'
        ], $messages);
        $user           = new User;
        $user->name     = $request->name;
        $user->email    = $request->email;
        $user->username = $request->username;
        $user->password = Hash::make($request->password);


        if($request->hasFile('image')) :
            $file = $request->image; 
            $name = str_random(5).$file->getClientOriginalName();
            $image_resize = Image::make($file->getRealPath());              
            $image_resize->resize(250, 250);
            $image_resize->save('image/backend/users/'.$name);
            $user->image = $name;
        else :
            $user->image = "user.png";
        endif;
        $user->save();
        $user->assignRole($request->input('roles'));

        return redirect()->route('users.index')->with('success','User created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        return view('backend.users.show',compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user     = User::find($id);
        $roles    = Role::pluck('name','name')->all();
        $userRole = $user->roles->pluck('name','name')->all();
        return view('backend.users.edit',compact('user', 'roles', 'userRole'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $messages = [
            'password.same' => 'พาสเวิร์ดยืนยันไม่ตรงกัน',
        ];
        $this->validate($request, [
            'name'     => 'required',
            'username' => 'required|unique:users,username,'.$id,
            'email'    => 'required|email|unique:users,email,'.$id,
            'password' => 'same:confirm-password',
            'roles'    => 'required'
        ], $messages);

        $user = User::find($id);
        $user->name     = $request->name;
        $user->email    = $request->email;
        $user->username = $request->username;
        // Check Null Password
        if (isset($request->password)) :
            $user->password = Hash::make($request->password);
        else:
            $user->password = $user->password;
        endif;
  
        if($request->hasFile('image')) :
            $request->edit_image == 'user.png' ? : File::delete('image/backend/user/'.$request->edit_image);
            $file = $request->image; 
            $name = str_random(5).$file->getClientOriginalName();
            $image_resize = Image::make($file->getRealPath());              
            $image_resize->resize(250, 250);
            $image_resize->save('image/backend/users/'.$name);
            $user->image = $name;
        else :
            $user->image = $request->edit_image;
        endif;
        $user->save();

        DB::table('model_has_roles')->where('model_id',$id)->delete();

        $user->assignRole($request->input('roles'));

        return redirect()->route('users.index')->with('success','User updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->image == 'user.png' ? : File::delete('image/backend/users/'.$user->image);
        User::destroy($user->id);
        return redirect()->route('users.index')->with('delete','User Deleted successfully');
    }
}

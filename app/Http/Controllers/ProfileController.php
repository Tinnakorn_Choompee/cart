<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Spatie\Permission\Models\Role;
use Intervention\Image\ImageManagerStatic as Image;
use DB;
use Hash;
use File;
use Auth;

class ProfileController extends Controller
{
    public function profile($id)
    {
        if(Auth::user()->id == $id):
            $user = User::find($id);
            return view('backend.profile.index')->withUser($user);
        else:
            return response()->view('404');
        endif;
    }

    public function edit($id)
    {
        if(Auth::user()->id == $id):
            $user     = User::find($id);
            $roles    = Role::pluck('name','name')->all();
            $userRole = $user->roles->pluck('name','name')->all();
            return view('backend.profile.edit',compact('user', 'roles', 'userRole'));
        else:
            return response()->view('404');
        endif;
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'     => 'required',
            'username' => 'required|unique:users,username,'.$id,
            'email'    => 'required|email|unique:users,email,'.$id,
            'password' => 'same:confirm-password',
        ]);

        $user = User::find($id);
        $user->name     = $request->name;
        $user->email    = $request->email;
        $user->username = $request->username;

        // Check Null Password
        if (isset($request->password)) :
            $user->password = Hash::make($request->password);
        else:
            $user->password = $user->password;
        endif;

        if($request->hasFile('image')) :
            $request->edit_image == 'user.png' ? : File::delete('image/backend/users/'.$request->edit_image);
            $file = $request->image; 
            $name = str_random(5).$file->getClientOriginalName();
            $image_resize = Image::make($file->getRealPath());              
            $image_resize->resize(250, 250);
            $image_resize->save('image/backend/users/'.$name);
            $user->image = $name;
        else :
            $user->image= $request->edit_image;
        endif;

        $user->save();

        DB::table('model_has_roles')->where('model_id',$id)->delete();

        $user->assignRole($request->input('roles'));

        return redirect()->route('profile', $id)->with('update','User updated successfully');
    }

    public function password(Request $request)
    {
        $message = ['same' => 'พาสเวิร์ดยืนยันไม่ตรงกัน'];
        $request->validate(['password_user' => 'required|same:password_confirmation'], $message);
        $user = User::find($request->id);
        $user->password = Hash::make($request->password_user);
        $user->save();
        return redirect()->back()->with('password_success', 'Change Successfully!');
    }
}

<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Type;
use App\Tag;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Intervention\Image\ImageManagerStatic as Image;
use File;
use Auth;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Backend.blog.post.index')->withBlogs(Blog::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $type = Type::all()->pluck('name', 'id');
        $tag  = Tag::all()->pluck('name', 'id');
        return view('Backend.blog.post.create')->withType($type)->withTag($tag);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|unique:blogs|max:255',
            'subtitle' => 'required',
            'slug' => 'required',
            'body' => 'required',
        ]);

        $blog = new Blog;
        $blog->title     = $request->title;
        $blog->subtitle  = $request->subtitle;
        $blog->slug      = $request->slug;
        $blog->body      = $request->body;
        $blog->posted_by = Auth::user()->name;

        if($request->hasFile('image')) :
            $file = $request->image; 
            $name = str_random(5).$file->getClientOriginalName();
            $image_resize = Image::make($file->getRealPath());              
            $image_resize->resize(500, 500);
            $image_resize->save('image/backend/blog/'.$name);
          
            // $thumbnailpath = 'image/backend/blog/thumbnail/'.$name;
            // $img = Image::make($file->getRealPath())->fit(300, 300);// Crop
            // $img->save($thumbnailpath);

            $blog->image = $name;
        else :
            $blog->image = "preview.png";
        endif;

        if ($request->has('public')) {
            $blog->status = 1;
        }

        $blog->save();
        $blog->tags()->sync($request->tag);
        $blog->types()->sync($request->type);

        return redirect()->route('blog.index')->with('success','Blog created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function show(Blog $blog)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function edit(Blog $blog)
    {
        $type = Type::all();
        $tag  = Tag::all();
        return view('Backend.blog.post.edit')->withTypes($type)->withTags($tag)->withBlog($blog);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Blog $blog)
    {
         $request->validate([
            'title' => 'required|unique:blogs,title,'.$blog->id,
            'subtitle' => 'required',
            'slug' => 'required',
            'body' => 'required',
        ]);

        $blog->title     = $request->title;
        $blog->subtitle  = $request->subtitle;
        $blog->slug      = $request->slug;
        $blog->body      = $request->body;
        $blog->posted_by = Auth::user()->name;
        
        if($request->hasFile('image')) :
            $request->edit_image == 'preview.png' ? : File::delete('image/backend/blog/'.$request->edit_image);
            $file = $request->image; 
            $name = str_random(5).$file->getClientOriginalName();
            $image_resize = Image::make($file->getRealPath());              
            $image_resize->resize(500, 500);
            $image_resize->save('image/backend/blog/'.$name);
          
            // $thumbnailpath = 'image/backend/blog/thumbnail/'.$name;
            // $img = Image::make($file->getRealPath())->fit(300, 300);// Crop
            // $img->save($thumbnailpath);

            $blog->image = $name;
        else :
            $blog->image = "preview.png";
        endif;

        if ($request->has('public')) {
            $blog->status = 1;
        }

        $blog->save();
        $blog->tags()->sync($request->tag);
        $blog->types()->sync($request->type);

        return redirect()->route('blog.index')->with('update','Blog updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function destroy(Blog $blog)
    {
       $blog->image == 'preview.png' ? : File::delete('image/backend/blog/'.$blog->image);
       Blog::destroy($blog->id);
       return redirect()->route('blog.index')->with('delete','Blog deleted successfully');
    }
}

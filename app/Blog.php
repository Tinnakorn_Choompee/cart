<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'blogtags');
    } 

    public function types()
    {
        return $this->belongsToMany(Type::class, 'blogtypes');
    } 
}

<?php
namespace Libraries\MenuLibrary;

class MenuLibrary
{
    public static function Role()
    {
        return [
                'user-list'  => 'ผู้ใช้งานระบบ' , 
                'role-list'  => 'กำหนดสิทธิ์ผู้ใช้งาน' , 
        ];
    }

    public static function Menu()
    {
        return [
                'dashboard' => ['route' => route('home') ,  'icon' => 'ion-android-color-palette', 'name'=> 'หน้าหลัก' , 'role' => 'index-list',  'url'=>'home' , 'submenu' => FALSE], 
                'blog'      => ['submenu'=> TRUE , 'title' => 'บทความ',
                                'name' => ['blog','type', 'tag'],
                                'list' => [
                                    'การโพสต์'      => ['route'=> route('blog.index') , 'url'=>'blog'],
                                    'ประเภทบทความ' => ['route'=> route('type.index') , 'url'=>'type'],
                                    'แท็กบทความ'   => ['route'=> route('tag.index')  , 'url'=>'tag'],
                                ],
                ], 
        ];
    }

    public static function Setting()
    {
        return [
                'facebook' => ['route' => route('facebook.index') ,  'icon' => 'fa-facebook-official', 'name'=> 'Facebook' , 'role' => 'facebook-list', 'url'=>'facebook'], 
        ];
    }

    public static function User()
    {
        return [
                'users' => ['route' => route('users.index') , 'icon' => 'fa-circle-o text-aqua', 'name'=> 'ผู้ใช้งานระบบ'      , 'role' => 'user-list' , 'url'=>'users'], 
                'roles' => ['route' => route('roles.index') , 'icon' => 'fa-circle-o text-aqua', 'name'=> 'กำหนดสิทธิ์ผู้ใช้งาน' , 'role' => 'role-list' , 'url'=>'roles'],
        ];
    }
}

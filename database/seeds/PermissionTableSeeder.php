<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            'role-list'   => 'ดู',
            'role-create' => 'เพิ่ม',
            'role-edit'   => 'แก้ไข',
            'role-delete' => 'ลบ',

            'user-list'   => 'ดู',
            'user-create' => 'เพิ่ม',
            'user-edit'   => 'แก้ไข',
            'user-delete' => 'ลบ',
         ];
 
         foreach ($permissions as $k => $permission) {
              Permission::create(['name' => $k, 'action' => $permission, 'guard_name'=>'web']);
         }
    }
}

<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => "Tinnakorn",
            'email' => 'tinnakorn@gmail.com',
            'username' => 'admin',
            'password' => bcrypt('123456'),
            'image' => 'user.png',
        ]);
    }
}


<?php
// ------------------- Frontend ---------------------------- //
// Home
Breadcrumbs::register('/', function ($breadcrumbs) {
    $breadcrumbs->push('Home', route('/'));
});
// Blog
Breadcrumbs::register('news', function ($breadcrumbs) {
    $breadcrumbs->parent('/');
    $breadcrumbs->push('news', route('news'));
});
// ---------------------------------------------------------- //


// ------------------- Backtend ----------------------------- //
// Home
Breadcrumbs::register('home', function ($breadcrumbs) {
    $breadcrumbs->push('หน้าหลัก', route('home'));
});

// User
Breadcrumbs::register('users.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('ผู้ใช้งานระบบ', route('users.index'));
});

Breadcrumbs::register('users.create', function ($breadcrumbs) {
    $breadcrumbs->parent('users.index');
    $breadcrumbs->push('เพิ่มผู้ใช้งานระบบ', route('users.create'));
});

Breadcrumbs::register('users.show', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('users.index');
    $breadcrumbs->push('ข้อมูลผู้ใช้งานระบบ', route('users.show', $id));
});

Breadcrumbs::register('users.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('users.index');
    $breadcrumbs->push('แก้ไขผู้ใช้งานระบบ', route('users.edit', $id));
});

// Profile
Breadcrumbs::register('profile', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('ข้อมูลส่วนตัว', route('profile', $id));
});
Breadcrumbs::register('profile.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('profile', $id);
    $breadcrumbs->push('แก้ไขข้อมูลส่วนตัว', route('profile.edit', $id));
});

// Roles
Breadcrumbs::register('roles.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('กำหนดสิทธิ์ผู้ใช้งาน', route('roles.index'));
});

Breadcrumbs::register('roles.create', function ($breadcrumbs) {
    $breadcrumbs->parent('roles.index');
    $breadcrumbs->push('เพิ่มสิทธิ์ผู้ใช้งาน', route('roles.create'));
});

Breadcrumbs::register('roles.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('roles.index');
    $breadcrumbs->push('แก้ไขสิทธิ์ผู้ใช้งาน', route('roles.edit', $id));
});

Breadcrumbs::register('roles.show', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('roles.index');
    $breadcrumbs->push('ดูสิทธิ์ผู้ใช้งาน', route('roles.show', $id));
});

// Facebook
Breadcrumbs::register('facebook.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('ตั้งค่าเฟสบุ๊ค', route('facebook.index'));
});

// Blog
Breadcrumbs::register('blog.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('การโพสต์', route('blog.index'));
});
Breadcrumbs::register('blog.create', function ($breadcrumbs) {
    $breadcrumbs->parent('blog.index');
    $breadcrumbs->push('เพิ่มการโพสต์', route('blog.create'));
});
Breadcrumbs::register('blog.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('blog.index');
    $breadcrumbs->push('แก้ไขการโพสต์', route('blog.edit', $id));
});

Breadcrumbs::register('blog.show', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('blog.index');
    $breadcrumbs->push('ดูข้อมูลการโพสต์', route('blog.show', $id));
});
// Type
Breadcrumbs::register('type.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('ประเภทบทความ', route('type.index'));
});
Breadcrumbs::register('type.create', function ($breadcrumbs) {
    $breadcrumbs->parent('type.index');
    $breadcrumbs->push('เพิ่มประเภทบทความ', route('type.create'));
});
Breadcrumbs::register('type.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('type.index');
    $breadcrumbs->push('แก้ไขประเภทบทความ', route('type.edit', $id));
});
// Tag
Breadcrumbs::register('tag.index', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('แท็กบทความ', route('tag.index'));
});
Breadcrumbs::register('tag.create', function ($breadcrumbs) {
    $breadcrumbs->parent('tag.index');
    $breadcrumbs->push('เพิ่มแท็กบทความ', route('tag.create'));
});
Breadcrumbs::register('tag.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('tag.index');
    $breadcrumbs->push('แก้ไขแท็กบทความ', route('tag.edit', $id));
});
// ------------------------------------------------------------------ //
?>
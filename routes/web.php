<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// ----------------------- Frontend --------------------------// 
Route::view('', 'Frontend.index')->name('/');
Route::view('detail', 'Frontend.detail')->name('detail');
Route::view('category', 'Frontend.category')->name('category');

Route::view('news', 'Frontend.blog')->name('news');
// ----------------------------------------------------------//



// ----------------------- Backtend --------------------------// 
Auth::routes();

Route::group(['middleware' => ['auth']], function() {
    // Home
    Route::get('home', 'HomeController@index')->name('home');
    // User
    Route::resource('users','UserController');
    // Profile
    Route::GET('profile/{id}' , 'ProfileController@profile')->name('profile');
    Route::GET('profile/{id}/edit', 'ProfileController@edit')->name('profile.edit');
    Route::PATCH('profile/{id}/update', 'ProfileController@update')->name('profile.update');
    // Password
    Route::PATCH('password' , 'ProfileController@password')->name('password');
    // Role
    Route::resource('roles','RoleController');
    // Facebook
    Route::resource('facebook','FacebookController');

    // Blog
    Route::resource('blog','BlogController');
    // Type
    Route::resource('type','TypeController');
    // Tag
    Route::resource('tag','TagController');
    
});
// ----------------------------------------------------------//